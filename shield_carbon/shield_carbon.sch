EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X15 P1
U 1 1 598F3013
P 1950 1650
F 0 "P1" H 1950 2450 50  0000 C CNN
F 1 "CONN_02X15" V 1950 1650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x15_Pitch2.54mm" H 1950 500 50  0000 C CNN
F 3 "" H 1950 500 50  0000 C CNN
	1    1950 1650
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X06 P5
U 1 1 598F316F
P 8900 3100
F 0 "P5" H 8900 3450 50  0000 C CNN
F 1 "IMU" V 9000 3100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 8900 3100 50  0000 C CNN
F 3 "" H 8900 3100 50  0000 C CNN
	1    8900 3100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 P6
U 1 1 598F31D0
P 8900 3800
F 0 "P6" H 8900 4050 50  0000 C CNN
F 1 "IMU_INT" V 9000 3800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 8900 3800 50  0000 C CNN
F 3 "" H 8900 3800 50  0000 C CNN
	1    8900 3800
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X07 P4
U 1 1 598F3DB9
P 8900 1500
F 0 "P4" H 8900 1900 50  0000 C CNN
F 1 "IR" V 9000 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 8900 1500 50  0000 C CNN
F 3 "" H 8900 1500 50  0000 C CNN
	1    8900 1500
	1    0    0    -1  
$EndComp
Text GLabel 1600 1450 0    60   Input ~ 0
Vcc_5V
Text GLabel 1600 1350 0    60   Input ~ 0
GND
Text GLabel 1600 1950 0    60   Input ~ 0
SCL
Text GLabel 1600 2050 0    60   Input ~ 0
SDA
Text GLabel 2300 1950 2    60   Input ~ 0
GYRO_INT2
Text GLabel 2300 2050 2    60   Input ~ 0
ACC_INT2
$Comp
L CONN_01X15 P2
U 1 1 598F47F3
P 2250 5350
F 0 "P2" H 2250 6150 50  0000 C CNN
F 1 "CONN_01X15" V 2350 5350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x15_Pitch2.54mm" H 2250 5350 50  0000 C CNN
F 3 "" H 2250 5350 50  0000 C CNN
	1    2250 5350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X15 P3
U 1 1 598F4919
P 2650 5350
F 0 "P3" H 2650 6150 50  0000 C CNN
F 1 "CONN_01X15" V 2750 5350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x15_Pitch2.54mm" H 2650 5350 50  0000 C CNN
F 3 "" H 2650 5350 50  0000 C CNN
	1    2650 5350
	-1   0    0    1   
$EndComp
Text GLabel 8600 1600 0    60   Input ~ 0
GND
Text GLabel 8600 1800 0    60   Input ~ 0
Vcc_5V
Text GLabel 8600 1200 0    60   Input ~ 0
SDA
Text GLabel 8600 1300 0    60   Input ~ 0
SCL
Text GLabel 8500 3150 0    60   Input ~ 0
GND
Text GLabel 8500 3350 0    60   Input ~ 0
Vcc_5V
Text GLabel 8500 2950 0    60   Input ~ 0
SDA
Text GLabel 8500 3050 0    60   Input ~ 0
SCL
Text GLabel 8500 3850 0    60   Input ~ 0
GYRO_INT2
Text GLabel 8500 3650 0    60   Input ~ 0
ACC_INT2
$Comp
L LED D1
U 1 1 598F5357
P 5050 5650
F 0 "D1" H 5050 5750 50  0000 C CNN
F 1 "LED" H 5050 5550 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5050 5650 50  0000 C CNN
F 3 "" H 5050 5650 50  0000 C CNN
	1    5050 5650
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 598F542C
P 5050 5150
F 0 "R1" V 5130 5150 50  0000 C CNN
F 1 "330" V 5050 5150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4980 5150 50  0000 C CNN
F 3 "" H 5050 5150 50  0000 C CNN
	1    5050 5150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 598F5577
P 5050 6050
F 0 "#PWR01" H 5050 5800 50  0001 C CNN
F 1 "GND" H 5050 5900 50  0000 C CNN
F 2 "" H 5050 6050 50  0000 C CNN
F 3 "" H 5050 6050 50  0000 C CNN
	1    5050 6050
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X07 P7
U 1 1 598F55EB
P 8900 5250
F 0 "P7" H 8900 5650 50  0000 C CNN
F 1 "BARO" V 9000 5250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.00mm" H 8900 5250 50  0000 C CNN
F 3 "" H 8900 5250 50  0000 C CNN
	1    8900 5250
	1    0    0    -1  
$EndComp
Text GLabel 8550 5350 0    60   Input ~ 0
GND
Text GLabel 8550 5550 0    60   Input ~ 0
Vcc_5V
Text GLabel 8550 5050 0    60   Input ~ 0
SDA
Text GLabel 8550 5250 0    60   Input ~ 0
SCL
$Comp
L LED D2
U 1 1 598F5A0D
P 5350 5650
F 0 "D2" H 5350 5750 50  0000 C CNN
F 1 "LED" H 5350 5550 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5350 5650 50  0000 C CNN
F 3 "" H 5350 5650 50  0000 C CNN
	1    5350 5650
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 598F5A13
P 5350 5150
F 0 "R2" V 5430 5150 50  0000 C CNN
F 1 "330" V 5350 5150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 5280 5150 50  0000 C CNN
F 3 "" H 5350 5150 50  0000 C CNN
	1    5350 5150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 598F5A19
P 5350 6050
F 0 "#PWR02" H 5350 5800 50  0001 C CNN
F 1 "GND" H 5350 5900 50  0000 C CNN
F 2 "" H 5350 6050 50  0000 C CNN
F 3 "" H 5350 6050 50  0000 C CNN
	1    5350 6050
	1    0    0    -1  
$EndComp
$Comp
L LED D3
U 1 1 598F5CDA
P 5650 5650
F 0 "D3" H 5650 5750 50  0000 C CNN
F 1 "LED" H 5650 5550 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5650 5650 50  0000 C CNN
F 3 "" H 5650 5650 50  0000 C CNN
	1    5650 5650
	0    -1   -1   0   
$EndComp
$Comp
L R R3
U 1 1 598F5CE0
P 5650 5150
F 0 "R3" V 5730 5150 50  0000 C CNN
F 1 "330" V 5650 5150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 5580 5150 50  0000 C CNN
F 3 "" H 5650 5150 50  0000 C CNN
	1    5650 5150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 598F5CE6
P 5650 6050
F 0 "#PWR03" H 5650 5800 50  0001 C CNN
F 1 "GND" H 5650 5900 50  0000 C CNN
F 2 "" H 5650 6050 50  0000 C CNN
F 3 "" H 5650 6050 50  0000 C CNN
	1    5650 6050
	1    0    0    -1  
$EndComp
$Comp
L LED D4
U 1 1 598F5CEE
P 5950 5650
F 0 "D4" H 5950 5750 50  0000 C CNN
F 1 "LED" H 5950 5550 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5950 5650 50  0000 C CNN
F 3 "" H 5950 5650 50  0000 C CNN
	1    5950 5650
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 598F5CF4
P 5950 5150
F 0 "R4" V 6030 5150 50  0000 C CNN
F 1 "330" V 5950 5150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 5880 5150 50  0000 C CNN
F 3 "" H 5950 5150 50  0000 C CNN
	1    5950 5150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 598F5CFA
P 5950 6050
F 0 "#PWR04" H 5950 5800 50  0001 C CNN
F 1 "GND" H 5950 5900 50  0000 C CNN
F 2 "" H 5950 6050 50  0000 C CNN
F 3 "" H 5950 6050 50  0000 C CNN
	1    5950 6050
	1    0    0    -1  
$EndComp
Text GLabel 1600 1750 0    60   Input ~ 0
PWM1
Text GLabel 2300 1750 2    60   Input ~ 0
PWM2
Text GLabel 1600 1850 0    60   Input ~ 0
PWM3
Text GLabel 2300 1850 2    60   Input ~ 0
PWM4
Text GLabel 5050 4850 1    60   Input ~ 0
PWM1
Text GLabel 5350 4850 1    60   Input ~ 0
PWM2
Text GLabel 5650 4850 1    60   Input ~ 0
PWM3
Text GLabel 5950 4850 1    60   Input ~ 0
PWM4
Text GLabel 1600 2150 0    60   Input ~ 0
GPIO_IR
Text GLabel 8600 1500 0    60   Input ~ 0
GPIO_IR
Text GLabel 1600 950  0    60   Input ~ 0
PA0/UART2_CTS
Text GLabel 1600 1050 0    60   Input ~ 0
UART_TX
Text GLabel 1600 1150 0    60   Input ~ 0
UART_RX
Text GLabel 1600 1250 0    60   Input ~ 0
PA1/UART2_RTS
Text GLabel 1600 1550 0    60   Input ~ 0
PC2/ADC_IN12
Text GLabel 1600 1650 0    60   Input ~ 0
PC4/ADC1_IN14
Text GLabel 1600 2250 0    60   Input ~ 0
PB10/PWM1/I2C2_SCL
Text GLabel 1600 2350 0    60   Input ~ 0
RST_BTN
Text GLabel 2300 950  2    60   Input ~ 0
PB12/SPI2_SS
Text GLabel 2300 1050 2    60   Input ~ 0
PB15/SPI2_MOSI
Text GLabel 2300 1150 2    60   Input ~ 0
PB14/SPI2_MISO
Text GLabel 2300 1250 2    60   Input ~ 0
PB13/SPI2_SCK
Text GLabel 2300 1350 2    60   Input ~ 0
GND
Text GLabel 2300 1450 2    60   Input ~ 0
VCC2
Text GLabel 2300 1550 2    60   Input ~ 0
PC3/ADC_IN13
Text GLabel 2300 1650 2    60   Input ~ 0
PC5/ADC1_IN15
Text GLabel 2300 2150 2    60   Input ~ 0
PC0/ADC_IN10
Text GLabel 2300 2250 2    60   Input ~ 0
PC1/ADC_IN11
Text GLabel 2300 2350 2    60   Input ~ 0
NC1
Text GLabel 1950 5150 0    60   Input ~ 0
Vcc_5V
Text GLabel 1950 5050 0    60   Input ~ 0
GND
Text GLabel 1950 5650 0    60   Input ~ 0
SCL
Text GLabel 1950 5750 0    60   Input ~ 0
SDA
Text GLabel 1950 5450 0    60   Input ~ 0
PWM1
Text GLabel 1950 5550 0    60   Input ~ 0
PWM3
Text GLabel 1950 5850 0    60   Input ~ 0
GPIO_IR
Text GLabel 1950 4650 0    60   Input ~ 0
PA0/UART2_CTS
Text GLabel 1950 4750 0    60   Input ~ 0
UART_TX
Text GLabel 1950 4850 0    60   Input ~ 0
UART_RX
Text GLabel 1950 4950 0    60   Input ~ 0
PA1/UART2_RTS
Text GLabel 1950 5250 0    60   Input ~ 0
PC2/ADC_IN12
Text GLabel 1950 5350 0    60   Input ~ 0
PC4/ADC1_IN14
Text GLabel 1950 5950 0    60   Input ~ 0
PB10/PWM1/I2C2_SCL
Text GLabel 1950 6050 0    60   Input ~ 0
RST_BTN
Text GLabel 2950 5650 2    60   Input ~ 0
GYRO_INT2
Text GLabel 2950 5750 2    60   Input ~ 0
ACC_INT2
Text GLabel 2950 5450 2    60   Input ~ 0
PWM2
Text GLabel 2950 5550 2    60   Input ~ 0
PWM4
Text GLabel 2950 4650 2    60   Input ~ 0
PB12/SPI2_SS
Text GLabel 2950 4750 2    60   Input ~ 0
PB15/SPI2_MOSI
Text GLabel 2950 4850 2    60   Input ~ 0
PB14/SPI2_MISO
Text GLabel 2950 4950 2    60   Input ~ 0
PB13/SPI2_SCK
Text GLabel 2950 5050 2    60   Input ~ 0
GND
Text GLabel 2950 5150 2    60   Input ~ 0
VCC2
Text GLabel 2950 5250 2    60   Input ~ 0
PC3/ADC_IN13
Text GLabel 2950 5350 2    60   Input ~ 0
PC5/ADC1_IN15
Text GLabel 2950 5850 2    60   Input ~ 0
PC0/ADC_IN10
Text GLabel 2950 5950 2    60   Input ~ 0
PC1/ADC_IN11
Text GLabel 2950 6050 2    60   Input ~ 0
NC1
Wire Wire Line
	1600 1350 1700 1350
Wire Wire Line
	1700 1450 1600 1450
Wire Wire Line
	1600 1950 1700 1950
Wire Wire Line
	1600 2050 1700 2050
Wire Wire Line
	2200 1950 2300 1950
Wire Wire Line
	2200 2050 2300 2050
Wire Wire Line
	8600 1800 8700 1800
Wire Wire Line
	8700 1600 8600 1600
Wire Wire Line
	8500 3350 8700 3350
Wire Wire Line
	8500 3150 8700 3150
Wire Wire Line
	8700 3050 8500 3050
Wire Wire Line
	8500 2950 8700 2950
Wire Wire Line
	8500 3650 8700 3650
Wire Wire Line
	8700 3850 8500 3850
Wire Wire Line
	8700 1300 8600 1300
Wire Wire Line
	8600 1200 8700 1200
Wire Wire Line
	8550 5050 8700 5050
Wire Wire Line
	8700 5250 8550 5250
Wire Wire Line
	8550 5350 8700 5350
Wire Wire Line
	8700 5550 8550 5550
Wire Wire Line
	5050 5300 5050 5450
Wire Wire Line
	5050 5850 5050 6050
Wire Wire Line
	5350 5300 5350 5450
Wire Wire Line
	5350 5850 5350 6050
Wire Wire Line
	5650 5300 5650 5450
Wire Wire Line
	5650 5850 5650 6050
Wire Wire Line
	5950 5300 5950 5450
Wire Wire Line
	5950 5850 5950 6050
Wire Wire Line
	1600 1750 1700 1750
Wire Wire Line
	1700 1850 1600 1850
Wire Wire Line
	2200 1750 2300 1750
Wire Wire Line
	2300 1850 2200 1850
Wire Wire Line
	5950 5000 5950 4850
Wire Wire Line
	5650 4850 5650 5000
Wire Wire Line
	5350 4850 5350 5000
Wire Wire Line
	5050 4850 5050 5000
Wire Wire Line
	8700 1500 8600 1500
Wire Wire Line
	1600 2150 1700 2150
Wire Wire Line
	2200 950  2300 950 
Wire Wire Line
	2300 1050 2200 1050
Wire Wire Line
	2300 1150 2200 1150
Wire Wire Line
	2300 1250 2200 1250
Wire Wire Line
	2200 1350 2300 1350
Wire Wire Line
	2300 1450 2200 1450
Wire Wire Line
	2200 1550 2300 1550
Wire Wire Line
	2300 1650 2200 1650
Wire Wire Line
	2200 2150 2300 2150
Wire Wire Line
	2300 2250 2200 2250
Wire Wire Line
	2200 2350 2300 2350
Wire Wire Line
	1700 2350 1600 2350
Wire Wire Line
	1600 2250 1700 2250
Wire Wire Line
	1600 1650 1700 1650
Wire Wire Line
	1700 1550 1600 1550
Wire Wire Line
	1600 1250 1700 1250
Wire Wire Line
	1700 1150 1600 1150
Wire Wire Line
	1600 1050 1700 1050
Wire Wire Line
	1700 950  1600 950 
Wire Wire Line
	1950 4650 2050 4650
Wire Wire Line
	2050 4750 1950 4750
Wire Wire Line
	1950 4850 2050 4850
Wire Wire Line
	2050 4950 1950 4950
Wire Wire Line
	1950 5050 2050 5050
Wire Wire Line
	2050 5150 1950 5150
Wire Wire Line
	1950 5250 2050 5250
Wire Wire Line
	2050 5350 1950 5350
Wire Wire Line
	1950 5450 2050 5450
Wire Wire Line
	2050 5550 1950 5550
Wire Wire Line
	1950 5650 2050 5650
Wire Wire Line
	2050 5750 1950 5750
Wire Wire Line
	1950 5850 2050 5850
Wire Wire Line
	2050 5950 1950 5950
Wire Wire Line
	1950 6050 2050 6050
Wire Wire Line
	2850 6050 2950 6050
Wire Wire Line
	2950 5950 2850 5950
Wire Wire Line
	2850 5850 2950 5850
Wire Wire Line
	2950 5750 2850 5750
Wire Wire Line
	2850 5650 2950 5650
Wire Wire Line
	2950 5550 2850 5550
Wire Wire Line
	2850 5450 2950 5450
Wire Wire Line
	2950 5350 2850 5350
Wire Wire Line
	2850 5250 2950 5250
Wire Wire Line
	2950 5150 2850 5150
Wire Wire Line
	2850 5050 2950 5050
Wire Wire Line
	2950 4950 2850 4950
Wire Wire Line
	2850 4850 2950 4850
Wire Wire Line
	2950 4750 2850 4750
Wire Wire Line
	2850 4650 2950 4650
$Comp
L CONN_01X15 P8
U 1 1 598FB878
P 5250 1900
F 0 "P8" H 5250 2700 50  0000 C CNN
F 1 "CONN_01X15" V 5350 1900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x15_Pitch2.54mm" H 5250 1900 50  0000 C CNN
F 3 "" H 5250 1900 50  0000 C CNN
	1    5250 1900
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X15 P9
U 1 1 598FB87E
P 5650 1900
F 0 "P9" H 5650 2700 50  0000 C CNN
F 1 "CONN_01X15" V 5750 1900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x15_Pitch2.54mm" H 5650 1900 50  0000 C CNN
F 3 "" H 5650 1900 50  0000 C CNN
	1    5650 1900
	-1   0    0    1   
$EndComp
Text GLabel 5950 2100 2    60   Input ~ 0
UART_TX
Text GLabel 5950 2200 2    60   Input ~ 0
UART_RX
Text GLabel 4950 2600 0    60   Input ~ 0
Vcc_5V
Text GLabel 4950 2500 0    60   Input ~ 0
GND
Wire Wire Line
	4950 2500 5050 2500
Wire Wire Line
	5050 2600 4950 2600
Wire Wire Line
	5850 2100 5950 2100
Wire Wire Line
	5850 2200 5950 2200
$EndSCHEMATC
