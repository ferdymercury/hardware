# uQA3 Hardware

The uQA3 project is an electronic engineering degree thesis from ''Universidad 
Tecnológica Nacional - Facultad Regional Córdoba'' (UTN-FRC), from Córdoba, Argentina.

In these files, you can find everything related to the hardware development from de uQA3 project,
like schematics, PCBs and 3D modeles.

## Getting Started

The uQA3 hardware has been developed using [KiCad-EDA](http://kicad-pcb.org/). 
KiCad is an open source software suite for Electronic Design Automation (EDA).
The programs handle Schematic Capture, and PCB Layout with Gerber output.
The suite runs on Windows, Linux and macOS and is licensed under GNU GPL v3.

To open the KiCad project you need KiCad v4.0.7 or higher.
You can download KiCad from its webpage: http://kicad-pcb.org/download/

## v1.0
Here is the ESP-WROOM-32 based hardware development of the uQA3.

In the [uqa3](uqa3) folder you can find two main folders:
- [esp32w](uqa3/esp32w): contains the KiCad project, schematics, gerber files, BOM and plots from the uQA3's PCB. 
- [libs](uqa3/libs): contains all the components libraries and 3D models needed to develop the PCB.

The [docs](docs) folder contains the datasheets from all the components used in the project and some schematics from several development boards. 

The [driver_motor](driver_motor) contains simulations in OrCad PSpice from the behavior of the motor driver and the transistors in the power input.

In the [shield_carbon](shiel_carbon) you can find a development board project developed in order to test several sensors and periferics from the Carbon BLE board.

In the [shield_wifi](shield_wifi) there is a development board project created to test the ESP-WROOM-32 development kit with the sensors and motors used in the uQA3.

![uqa3v1.0](https://gitlab.com/uQA3/hardware/raw/master/uqa3/esp32w/Img/uQA3_iso.png)

## v0.1
Here is the STM32F401 microcontroller based development of the uQA3.

The [uqa3](uqa3) contains the KiCad project, schematics, gerber files, BOM and plots from the uQA3's PCB. 

The components libraries and 3D models needed to develop the PCB are located in the [kicad](kicad) folder.

The [docs](docs) folder contains the datasheets from all the components used in the v0.1 
and some schematics from several development boards. 

![uqa3v0.1](https://gitlab.com/uQA3/hardware/blob/ruteo2/uqa3/uqa3v01.png)

## Authors
* **Tomás Elías**
* **Diego Hernando**
* **Iñaki Malerba**
* **Joaquín Miranda**
* **Claudio Paz**

## Contributors
* **Centro de Investigación en Infromática para la Ingeniería** - [CIII](http://ciii.frc.utn.edu.ar) 
* **Cámara de Industrias Informáticas, Electrónicas y de Comunicaciones del Centro de Argentina** - [CIIECCA](http://www.ciiecca.org.ar/)
