PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
TPS63001
$EndINDEX
$MODULE TPS63001
Po 0 0 0 15 00000000 00000000 ~~
Li TPS63001
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 0.5256 -3.1064 0.64 0.64 0 0.05 N V 21 "TPS63001"
T1 0 0 1 0.9 0 0.05 N H 21 "VAL**"
DS -1.4 -1.6 -1.6 -1.6 0.127 21
DS -1.6 -1.6 -1.6 -0.8 0.127 21
DS -1.6 1.2 -1.6 0.8 0.127 21
DS -1.6 1.2 -1.2 1.6 0.127 21
DS 1.6 -1.6 1.6 -0.8 0.127 21
DS 1.6 0.8 1.6 1.6 0.127 21
DS 1.6 -1.6 1.4 -1.6 0.127 21
DS 1.6 1.6 1.4 1.6 0.127 21
DS -1.6 0.8 -1.6 -0.8 0.127 24
DS 1.6 0.8 1.6 -0.8 0.127 24
DS 1.4 -1.6 -1.4 -1.6 0.127 24
DS 1.4 1.6 -1.2 1.6 0.127 24
DP 0 0 0 0 4 0.381 19
Dl 0.1 -0.7
Dl 1.1 -0.7
Dl 1.1 -0.1
Dl 0.1 -0.1
DP 0 0 0 0 4 0.381 19
Dl -1.1 -0.7
Dl -0.1 -0.7
Dl -0.1 -0.1
Dl -1.1 -0.1
DP 0 0 0 0 4 0.381 19
Dl -1.1 0.1
Dl -0.1 0.1
Dl -0.1 0.7
Dl -1.1 0.7
DP 0 0 0 0 4 0.381 19
Dl 0.1 0.1
Dl 1.1 0.1
Dl 1.1 0.7
Dl 0.1 0.7
$PAD
Sh "10" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1 -1.475
$EndPAD
$PAD
Sh "9" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.5 -1.475
$EndPAD
$PAD
Sh "8" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0 -1.475
$EndPAD
$PAD
Sh "7" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.5 -1.475
$EndPAD
$PAD
Sh "6" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1 -1.475
$EndPAD
$PAD
Sh "5" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1 1.475
$EndPAD
$PAD
Sh "4" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.5 1.475
$EndPAD
$PAD
Sh "3" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0 1.475
$EndPAD
$PAD
Sh "2" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.5 1.475
$EndPAD
$PAD
Sh "1" R 0.85 0.28 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1 1.475
$EndPAD
$PAD
Sh "PGND3" R 0.7 0.28 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.55 0.25
$EndPAD
$PAD
Sh "PGND2" R 0.7 0.28 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.55 -0.25
$EndPAD
$PAD
Sh "PGND1" R 0.7 0.28 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.55 -0.25
$EndPAD
$PAD
Sh "PGND4" R 0.7 0.28 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.55 0.25
$EndPAD
$PAD
Sh "PAD" R 2.4 1.65 0 0 0
At SMD N 00888000
.SolderMask 0
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE TPS63001
