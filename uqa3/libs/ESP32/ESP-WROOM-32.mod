PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
MODULE_ESP-WROOM-32
$EndINDEX
$MODULE MODULE_ESP-WROOM-32
Po 0 0 0 15 00000000 00000000 ~~
Li MODULE_ESP-WROOM-32
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -5.67553 -17.174 1.32663 1.32663 0 0.05 N V 21 "MODULE_ESP-WROOM-32"
T1 -4.83689 11.7296 1.32768 1.32768 0 0.05 N V 21 "VAL**"
DS -9 9.755 9 9.755 0.127 24
DS 9 9.755 9 -15.745 0.127 24
DS 9 -15.745 -9 -15.745 0.127 24
DS -9 -15.745 -9 9.755 0.127 24
DS -9 9 -9 9.75 0.127 21
DS -9 9.75 -6.5 9.75 0.127 21
DS 6.5 9.75 9 9.75 0.127 21
DS 9 9.75 9 9 0.127 21
DS -9 -9.25 -9 -15.75 0.127 21
DS -9 -15.75 9 -15.75 0.127 21
DS 9 -15.75 9 -9.25 0.127 21
DP 0 0 0 0 4 0.381 21
Dl -8.02361 -15
Dl -7.5 -15
Dl -7.5 -10.531
Dl -8.02361 -10.531
DP 0 0 0 0 4 0.381 21
Dl -8.01655 -15
Dl -5 -15
Dl -5 -14.53
Dl -8.01655 -14.53
DP 0 0 0 0 4 0.381 21
Dl -5.52668 -14.5
Dl -5 -14.5
Dl -5 -12.0583
Dl -5.52668 -12.0583
DP 0 0 0 0 4 0.381 21
Dl -5.51685 -12.5
Dl -2.5 -12.5
Dl -2.5 -12.0368
Dl -5.51685 -12.0368
DP 0 0 0 0 4 0.381 21
Dl -3.01062 -14.5
Dl -2.5 -14.5
Dl -2.5 -12.0423
Dl -3.01062 -12.0423
DP 0 0 0 0 4 0.381 21
Dl -3.01385 -15
Dl 0 -15
Dl 0 -14.5668
Dl -3.01385 -14.5668
DP 0 0 0 0 4 0.381 21
Dl -0.502042 -14.5
Dl 0 -14.5
Dl 0 -12.049
Dl -0.502042 -12.049
DP 0 0 0 0 4 0.381 21
Dl -0.502218 -12.5
Dl 2.5 -12.5
Dl 2.5 -12.0532
Dl -0.502218 -12.0532
DP 0 0 0 0 4 0.381 21
Dl 2.01184 -14.5
Dl 2.5 -14.5
Dl 2.5 -12.071
Dl 2.01184 -12.071
DP 0 0 0 0 4 0.381 21
Dl 2.01243 -15
Dl 7.5 -15
Dl 7.5 -14.5199
Dl 2.01243 -14.5199
DP 0 0 0 0 4 0.381 21
Dl 7.03468 -15
Dl 7.5 -15
Dl 7.5 -10.0496
Dl 7.03468 -10.0496
DP 0 0 0 0 4 0.381 21
Dl 4.51568 -15
Dl 5 -15
Dl 5 -10.0349
Dl 4.51568 -10.0349
DP 0 0 0 0 4 0.381 21
Dl 7.02518 -10
Dl 7.5 -10
Dl 7.5 -9.53417
Dl 7.02518 -9.53417
DP 0 0 0 0 4 0.381 21
Dl 4.51397 -10
Dl 5 -10
Dl 5 -9.52947
Dl 4.51397 -9.52947
DP 0 0 0 0 4 0.381 21
Dl -8.02468 -10.5
Dl -7.5 -10.5
Dl -7.5 -10.0308
Dl -8.02468 -10.0308
DP 0 0 0 0 4 0.381 21
Dl 2.01098 -15
Dl 2.5 -15
Dl 2.5 -13.5741
Dl 2.01098 -13.5741
DP 0 0 0 0 4 0.381 24
Dl -8.05381 -15
Dl -7.5 -15
Dl -7.5 -10.5706
Dl -8.05381 -10.5706
DP 0 0 0 0 4 0.381 24
Dl -8.0414 -15
Dl -5 -15
Dl -5 -14.5751
Dl -8.0414 -14.5751
DP 0 0 0 0 4 0.381 24
Dl -5.52694 -14.5
Dl -5 -14.5
Dl -5 -12.0587
Dl -5.52694 -12.0587
DP 0 0 0 0 4 0.381 24
Dl -5.52302 -12.5
Dl -2.5 -12.5
Dl -2.5 -12.0503
Dl -5.52302 -12.0503
DP 0 0 0 0 4 0.381 24
Dl -3.01227 -14.5
Dl -2.5 -14.5
Dl -2.5 -12.0491
Dl -3.01227 -12.0491
DP 0 0 0 0 4 0.381 24
Dl -3.00983 -15
Dl 0 -15
Dl 0 -14.5476
Dl -3.00983 -14.5476
DP 0 0 0 0 4 0.381 24
Dl -0.502067 -14.5
Dl 0 -14.5
Dl 0 -12.0497
Dl -0.502067 -12.0497
DP 0 0 0 0 4 0.381 24
Dl -0.501099 -12.5
Dl 2.5 -12.5
Dl 2.5 -12.0263
Dl -0.501099 -12.0263
DP 0 0 0 0 4 0.381 24
Dl 2.01165 -14.5
Dl 2.5 -14.5
Dl 2.5 -12.0699
Dl 2.01165 -12.0699
DP 0 0 0 0 4 0.381 24
Dl 2.01848 -15
Dl 7.5 -15
Dl 7.5 -14.5637
Dl 2.01848 -14.5637
DP 0 0 0 0 4 0.381 24
Dl 7.03773 -15
Dl 7.5 -15
Dl 7.5 -10.0539
Dl 7.03773 -10.0539
DP 0 0 0 0 4 0.381 24
Dl 4.51883 -15
Dl 5 -15
Dl 5 -10.042
Dl 4.51883 -10.042
DP 0 0 0 0 4 0.381 24
Dl 7.02353 -10
Dl 7.5 -10
Dl 7.5 -9.53194
Dl 7.02353 -9.53194
DP 0 0 0 0 4 0.381 24
Dl 4.52306 -10
Dl 5 -10
Dl 5 -9.5487
Dl 4.52306 -9.5487
DP 0 0 0 0 4 0.381 24
Dl -8.03752 -10.5
Dl -7.5 -10.5
Dl -7.5 -10.047
Dl -8.03752 -10.047
DP 0 0 0 0 4 0.381 24
Dl 2.01117 -15
Dl 2.5 -15
Dl 2.5 -13.5755
Dl 2.01117 -13.5755
DS -9.25 -16 9.25 -16 0.05 24
DS 9.25 -16 9.25 -9 0.05 24
DS 9.25 -9 10.1 -9 0.05 24
DS 10.1 -9 10.1 9 0.05 24
DS 10.1 9 9.25 9 0.05 24
DS 9.25 9 9.25 10 0.05 24
DS 9.25 10 6.5 10 0.05 24
DS 6.5 10 6.5 10.855 0.05 24
DS 6.5 10.855 -6.5 10.855 0.05 24
DS -6.5 10.855 -6.5 10 0.05 24
DS -6.5 10 -9.25 10 0.05 24
DS -9.25 10 -9.25 9 0.05 24
DS -9.25 9 -10.1 9 0.05 24
DS -10.1 9 -10.1 -9 0.05 24
DS -10.1 -9 -9.25 -9 0.05 24
DS -9.25 -9 -9.25 -16 0.05 24
DP 0 0 0 0 4 0.381 24
Dl -9.03944 -15.75
Dl 9 -15.75
Dl 9 -9.03944
Dl -9.03944 -9.03944
DP 0 0 0 0 4 0.381 24
Dl -9.01722 -15.75
Dl 9 -15.75
Dl 9 -9.01722
Dl -9.01722 -9.01722
DP 0 0 0 0 4 0.381 24
Dl -9.03914 -15.75
Dl 9 -15.75
Dl 9 -9.03914
Dl -9.03914 -9.03914
DC -10.365 -8.2754 -10.265 -8.2754 0.2 21
DC -7.425 -8.2754 -7.325 -8.2754 0.2 24
$PAD
Sh "38" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 -8.255
$EndPAD
$PAD
Sh "37" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 -6.985
$EndPAD
$PAD
Sh "36" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 -5.715
$EndPAD
$PAD
Sh "35" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 -4.445
$EndPAD
$PAD
Sh "34" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 -3.175
$EndPAD
$PAD
Sh "33" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 -1.905
$EndPAD
$PAD
Sh "32" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 -0.635
$EndPAD
$PAD
Sh "31" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 0.635
$EndPAD
$PAD
Sh "30" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 1.905
$EndPAD
$PAD
Sh "29" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 3.175
$EndPAD
$PAD
Sh "28" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 4.445
$EndPAD
$PAD
Sh "27" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 5.715
$EndPAD
$PAD
Sh "26" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 6.985
$EndPAD
$PAD
Sh "25" R 1.7 0.9 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9 8.255
$EndPAD
$PAD
Sh "24" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.715 9.755
$EndPAD
$PAD
Sh "23" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.445 9.755
$EndPAD
$PAD
Sh "22" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.175 9.755
$EndPAD
$PAD
Sh "21" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.905 9.755
$EndPAD
$PAD
Sh "20" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.635 9.755
$EndPAD
$PAD
Sh "19" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.635 9.755
$EndPAD
$PAD
Sh "18" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.905 9.755
$EndPAD
$PAD
Sh "17" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.175 9.755
$EndPAD
$PAD
Sh "16" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.445 9.755
$EndPAD
$PAD
Sh "15" R 1.7 0.9 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.715 9.755
$EndPAD
$PAD
Sh "14" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 8.255
$EndPAD
$PAD
Sh "13" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 6.985
$EndPAD
$PAD
Sh "12" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 5.715
$EndPAD
$PAD
Sh "11" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 4.445
$EndPAD
$PAD
Sh "10" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 3.175
$EndPAD
$PAD
Sh "9" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 1.905
$EndPAD
$PAD
Sh "8" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 0.635
$EndPAD
$PAD
Sh "7" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 -0.635
$EndPAD
$PAD
Sh "6" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 -1.905
$EndPAD
$PAD
Sh "5" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 -3.175
$EndPAD
$PAD
Sh "4" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 -4.445
$EndPAD
$PAD
Sh "3" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 -5.715
$EndPAD
$PAD
Sh "2" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 -6.985
$EndPAD
$PAD
Sh "1" R 1.7 0.9 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9 -8.255
$EndPAD
$PAD
Sh "1.1" R 4 4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.7 -0.845
$EndPAD
$EndMODULE MODULE_ESP-WROOM-32
