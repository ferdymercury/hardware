EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Adafruit BME280
LIBS:ESP32-PICO-D4
LIBS:FXAS21002C+FXOS8700CQ
LIBS:Adafruit MicroUSB Lipo
LIBS:MPU6050
LIBS:TPS63001
LIBS:Adafruit VL53L0X
LIBS:cp2102
LIBS:switches
LIBS:motors
LIBS:MPU-9250
LIBS:ESP-WROOM-32
LIBS:uQA3-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "IMU + Baro + Láser"
Date "2018-07-18"
Rev ""
Comp "Claudio J. Paz"
Comment1 "Cuadrotor aéreo no tripulado "
Comment2 "de dimensiones pequeñas "
Comment3 "y estructura abierta uQA3"
Comment4 "2018"
$EndDescr
Text Notes 700  3050 0    60   ~ 0
ACELERÓMETRO, GIRÓSCOPO Y MAGNETÓMETRO
Text Notes 700  2950 0    60   ~ 0
IMU
$Comp
L VL53L0X U6
U 1 1 5980D49B
P 9100 2050
F 0 "U6" H 8600 2669 45  0000 L BNN
F 1 "VL53L0X" H 8600 1359 45  0000 L BNN
F 2 "VL53L0X:VL53L0X_2" H 9130 2200 20  0001 C CNN
F 3 "" H 9100 2050 60  0001 C CNN
	1    9100 2050
	1    0    0    -1  
$EndComp
$Comp
L R R27
U 1 1 5980DDBB
P 8400 1500
F 0 "R27" V 8300 1500 50  0000 C CNN
F 1 "10K" V 8400 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8330 1500 50  0001 C CNN
F 3 "" H 8400 1500 50  0001 C CNN
	1    8400 1500
	-1   0    0    1   
$EndComp
$Comp
L R R25
U 1 1 5980DE57
P 8200 1500
F 0 "R25" V 8280 1500 50  0000 C CNN
F 1 "10K" V 8200 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8130 1500 50  0001 C CNN
F 3 "" H 8200 1500 50  0001 C CNN
	1    8200 1500
	1    0    0    -1  
$EndComp
Text GLabel 8350 1950 0    60   Output ~ 0
SDA_2.8V
Text GLabel 8350 2050 0    60   Output ~ 0
SCL_2.8V
$Comp
L C C14
U 1 1 5980F485
P 10050 2000
F 0 "C14" H 10075 2100 50  0000 L CNN
F 1 "0.1uF" H 10075 1900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 10088 1850 50  0001 C CNN
F 3 "" H 10050 2000 50  0001 C CNN
	1    10050 2000
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR046
U 1 1 5980F725
P 10050 2450
F 0 "#PWR046" H 10050 2200 50  0001 C CNN
F 1 "Earth" H 10050 2300 50  0001 C CNN
F 2 "" H 10050 2450 50  0001 C CNN
F 3 "" H 10050 2450 50  0001 C CNN
	1    10050 2450
	1    0    0    -1  
$EndComp
NoConn ~ 8500 2350
$Comp
L R R24
U 1 1 59810E67
P 8100 4500
F 0 "R24" V 8180 4500 50  0000 C CNN
F 1 "10K" V 8100 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8030 4500 50  0001 C CNN
F 3 "" H 8100 4500 50  0001 C CNN
	1    8100 4500
	1    0    0    -1  
$EndComp
$Comp
L R R26
U 1 1 59810EE9
P 8350 4500
F 0 "R26" V 8430 4500 50  0000 C CNN
F 1 "10K" V 8350 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8280 4500 50  0001 C CNN
F 3 "" H 8350 4500 50  0001 C CNN
	1    8350 4500
	1    0    0    -1  
$EndComp
$Comp
L R R20
U 1 1 59810F5B
P 9550 4500
F 0 "R20" V 9630 4500 50  0000 C CNN
F 1 "10K" V 9550 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 9480 4500 50  0001 C CNN
F 3 "" H 9550 4500 50  0001 C CNN
	1    9550 4500
	1    0    0    -1  
$EndComp
$Comp
L R R21
U 1 1 59810FF3
P 9800 4500
F 0 "R21" V 9880 4500 50  0000 C CNN
F 1 "10K" V 9800 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 9730 4500 50  0001 C CNN
F 3 "" H 9800 4500 50  0001 C CNN
	1    9800 4500
	1    0    0    -1  
$EndComp
Text GLabel 7950 5150 0    60   Input ~ 0
SDA_2.8V
Text GLabel 7950 4750 0    60   Input ~ 0
SCL_2.8V
Text GLabel 2000 4350 0    60   BiDi ~ 0
SENSORES_SDA
Text GLabel 10000 4750 2    60   Output ~ 0
SENSORES_SCL
Text GLabel 10000 5150 2    60   Output ~ 0
SENSORES_SDA
Text Notes 7200 750  0    60   ~ 0
SENSOR LASER
Text Notes 7200 3400 0    60   ~ 0
ADAPTACIÓN NIVELES I2C
Text HLabel 1300 1650 0    60   Input ~ 0
3.3V
Text GLabel 1500 1650 2    60   Output ~ 0
3.3V
Text HLabel 1300 1800 0    60   Input ~ 0
GND
$Comp
L Earth #PWR047
U 1 1 5997C38B
P 1500 1900
F 0 "#PWR047" H 1500 1650 50  0001 C CNN
F 1 "Earth" H 1500 1750 50  0001 C CNN
F 2 "" H 1500 1900 50  0001 C CNN
F 3 "" H 1500 1900 50  0001 C CNN
	1    1500 1900
	1    0    0    -1  
$EndComp
Text HLabel 3600 1950 0    60   BiDi ~ 0
SENSORES_SDA
Text HLabel 3600 2050 0    60   BiDi ~ 0
SENSORES_SCL
Text GLabel 3800 2050 2    60   BiDi ~ 0
SENSORES_SCL
Text GLabel 3800 1950 2    60   BiDi ~ 0
SENSORES_SDA
Text GLabel 3750 1450 2    60   Input ~ 0
IMU_INT1
Text HLabel 3550 1450 0    60   Output ~ 0
IMU_INT1
Text GLabel 5300 3650 2    60   Input ~ 0
3.3V
Text HLabel 5450 1450 0    60   Input ~ 0
BARO_ADR
Text GLabel 5650 1450 2    60   Output ~ 0
BARO_ADR
Text HLabel 1300 1500 0    60   Input ~ 0
2.8V
Text GLabel 1500 1500 2    60   Output ~ 0
2.8V
Text GLabel 8100 1250 0    60   Input ~ 0
2.8V
Text GLabel 10150 1550 2    60   Input ~ 0
2.8V
Text GLabel 8000 4100 0    60   Input ~ 0
2.8V
Text GLabel 9900 4100 2    60   Input ~ 0
3.3V
Text Notes 7250 5800 0    60   ~ 0
NOTA: En caso de usar el sensor láser con alimentación 3.3V,\nreemplazar todas las conexiones de 2.8V por 3.3V y puentear\nlos transistores de forma tal que queden conectados SDA y SCL.
Text Notes 700  750  0    60   ~ 0
REFERENCIAS
Text GLabel 7900 1850 0    60   Input ~ 0
GPIO_LASER
Text HLabel 5450 2150 0    60   Input ~ 0
GPIO_LASER
Text GLabel 5650 2150 2    60   Output ~ 0
GPIO_LASER
Text GLabel 5650 2000 2    60   Output ~ 0
XSHUT
Text HLabel 5450 2000 0    60   Input ~ 0
XSHUT
Text GLabel 7650 1750 0    60   Input ~ 0
XSHUT
Wire Notes Line
	11100 600  7100 600 
Wire Notes Line
	11100 600  11100 2850
Wire Notes Line
	7100 600  7100 5950
Wire Notes Line
	7100 5950 11100 5950
Wire Notes Line
	11100 5950 11100 2800
Wire Wire Line
	8200 1250 8200 1350
Wire Wire Line
	8350 2050 8500 2050
Wire Wire Line
	8500 1950 8350 1950
Wire Wire Line
	9800 2350 9700 2350
Wire Wire Line
	9800 1950 9800 2350
Wire Wire Line
	9700 2250 10050 2250
Wire Wire Line
	9800 2150 9700 2150
Connection ~ 9800 2250
Wire Wire Line
	9800 2050 9700 2050
Connection ~ 9800 2150
Wire Wire Line
	9800 1950 9700 1950
Connection ~ 9800 2050
Wire Wire Line
	9700 1850 9800 1850
Wire Wire Line
	9800 1850 9800 1750
Wire Wire Line
	9700 1750 10050 1750
Wire Wire Line
	10050 2450 10050 2150
Wire Wire Line
	10050 1850 10050 1550
Connection ~ 10050 1750
Connection ~ 9800 1750
Connection ~ 10050 2250
Wire Wire Line
	8100 4650 8100 4750
Wire Wire Line
	7950 4750 8500 4750
Wire Wire Line
	8350 4650 8350 5150
Wire Wire Line
	7950 5150 9000 5150
Wire Wire Line
	8350 4350 8350 4250
Wire Wire Line
	8100 4250 9200 4250
Wire Wire Line
	8100 4100 8100 4350
Connection ~ 8350 4250
Connection ~ 8100 4250
Connection ~ 8100 4750
Connection ~ 8350 5150
Wire Wire Line
	9550 4650 9550 4750
Wire Wire Line
	8900 4750 10000 4750
Wire Wire Line
	9800 4650 9800 5150
Wire Wire Line
	9400 5150 10000 5150
Wire Wire Line
	9550 4350 9550 4250
Wire Wire Line
	9550 4250 9800 4250
Wire Wire Line
	9800 4100 9800 4350
Connection ~ 9800 4250
Connection ~ 9550 4750
Connection ~ 9800 5150
Wire Notes Line
	7100 3250 11100 3250
Wire Wire Line
	1300 1650 1500 1650
Wire Wire Line
	1300 1800 1500 1800
Wire Wire Line
	1500 1800 1500 1900
Wire Wire Line
	3800 2050 3600 2050
Wire Wire Line
	3600 1950 3800 1950
Wire Wire Line
	3550 1450 3750 1450
Wire Wire Line
	5450 1450 5650 1450
Wire Wire Line
	1300 1500 1500 1500
Wire Wire Line
	8100 1250 8400 1250
Wire Wire Line
	5650 2000 5450 2000
Wire Wire Line
	10050 1550 10150 1550
Wire Wire Line
	8000 4100 8100 4100
Wire Wire Line
	9900 4100 9800 4100
Wire Notes Line
	6950 7650 600  7650
Wire Notes Line
	600  2750 600  600 
Wire Notes Line
	600  600  6950 600 
Wire Notes Line
	600  7650 600  2700
Wire Wire Line
	8400 1650 8400 1850
Connection ~ 8400 1850
Wire Wire Line
	8400 1250 8400 1350
Connection ~ 8200 1250
Wire Wire Line
	7900 1850 8500 1850
Wire Wire Line
	5650 2150 5450 2150
Connection ~ 8200 1750
Wire Wire Line
	8200 1650 8200 1750
Wire Wire Line
	8100 1750 8500 1750
Wire Wire Line
	7800 1750 7650 1750
$Comp
L Q_NMOS_GSD Q9
U 1 1 5B0F6423
P 8700 4650
F 0 "Q9" V 8900 4550 50  0000 L CNN
F 1 "BSS138" V 9000 4550 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 8900 4750 50  0001 C CNN
F 3 "" H 8700 4650 50  0001 C CNN
	1    8700 4650
	0    1    1    0   
$EndComp
$Comp
L Q_NMOS_GSD Q10
U 1 1 5B0F688B
P 9200 5050
F 0 "Q10" V 9450 5100 50  0000 L CNN
F 1 "BSS138" V 9450 4700 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 9400 5150 50  0001 C CNN
F 3 "" H 9200 5050 50  0001 C CNN
	1    9200 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	8700 4450 8700 4250
Connection ~ 8700 4250
Wire Wire Line
	9200 4250 9200 4850
$Comp
L D D14
U 1 1 5B0F746C
P 7950 1750
F 0 "D14" H 7700 1650 50  0000 C CNN
F 1 "1N4148" H 7950 1650 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 7950 1750 50  0001 C CNN
F 3 "" H 7950 1750 50  0001 C CNN
	1    7950 1750
	1    0    0    1   
$EndComp
Text GLabel 2000 4250 0    60   BiDi ~ 0
SENSORES_SCL
Text Notes 700  5750 0    60   ~ 0
BARÓMETRO
Wire Wire Line
	2850 6550 2650 6550
Wire Wire Line
	3350 6550 3150 6550
Wire Wire Line
	2750 5950 2850 5950
Connection ~ 2750 6450
Wire Wire Line
	2750 6450 2750 6350
Wire Wire Line
	2650 6450 2850 6450
Wire Wire Line
	2750 6050 2750 5950
Wire Wire Line
	2650 6850 2850 6850
Wire Wire Line
	2650 6750 2850 6750
Wire Wire Line
	1550 6850 1550 7100
Wire Wire Line
	1650 6850 1550 6850
Wire Wire Line
	1550 6550 1650 6550
Wire Wire Line
	1650 6450 1550 6450
Text Notes 1750 7250 0    60   ~ 0
Address: 0x77
Text Notes 4100 6700 0    60   ~ 0
NOTA: Para SPI, CSB bajo en startup, SDO=MISO, \nSDI=MOSI, SCK=CLK, CSB=CS/SSEL\n      Para I2C, CBS pull up. SDI=SDA, SCK=SCL
Text GLabel 3350 6550 2    60   Input ~ 0
3.3V
Text GLabel 2850 5950 2    60   Input ~ 0
3.3V
Text GLabel 1550 6550 0    60   Input ~ 0
3.3V
Text GLabel 1550 6450 0    60   Input ~ 0
3.3V
Text GLabel 2850 6850 2    60   Output ~ 0
SENSORES_SDA
Text GLabel 2850 6750 2    60   Output ~ 0
SENSORES_SCL
Text GLabel 2850 6450 2    60   Input ~ 0
BARO_ADR
$Comp
L R R23
U 1 1 5960F748
P 3000 6550
F 0 "R23" V 3080 6550 50  0000 C CNN
F 1 "10K" V 3000 6550 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2930 6550 50  0001 C CNN
F 3 "" H 3000 6550 50  0001 C CNN
	1    3000 6550
	0    1    1    0   
$EndComp
$Comp
L R R22
U 1 1 5960F4BC
P 2750 6200
F 0 "R22" V 2830 6200 50  0000 C CNN
F 1 "10K" V 2750 6200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2680 6200 50  0001 C CNN
F 3 "" H 2750 6200 50  0001 C CNN
	1    2750 6200
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR048
U 1 1 5960E662
P 1550 7100
F 0 "#PWR048" H 1550 6850 50  0001 C CNN
F 1 "Earth" H 1550 6950 50  0001 C CNN
F 2 "" H 1550 7100 50  0001 C CNN
F 3 "" H 1550 7100 50  0001 C CNN
	1    1550 7100
	1    0    0    -1  
$EndComp
$Comp
L BME280 U4
U 1 1 5960DD9B
P 2150 6650
F 0 "U4" H 1750 7200 45  0000 L BNN
F 1 "BME280" H 2300 7200 45  0000 L BNN
F 2 "BME280:BME280" H 2180 6800 20  0001 C CNN
F 3 "" H 2150 6650 60  0001 C CNN
	1    2150 6650
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 5B10D4FC
P 5300 4650
F 0 "C12" H 5325 4750 50  0000 L CNN
F 1 "0.1uF" H 5325 4550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5338 4500 50  0001 C CNN
F 3 "" H 5300 4650 50  0001 C CNN
	1    5300 4650
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 5B10D632
P 5400 3300
F 0 "C13" H 5425 3400 50  0000 L CNN
F 1 "0.1uF" H 5425 3200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5438 3150 50  0001 C CNN
F 3 "" H 5400 3300 50  0001 C CNN
	1    5400 3300
	0    -1   1    0   
$EndComp
$Comp
L GND #PWR049
U 1 1 5B10E62D
P 5300 5000
F 0 "#PWR049" H 5300 4750 50  0001 C CNN
F 1 "GND" H 5300 4850 50  0000 C CNN
F 2 "" H 5300 5000 50  0001 C CNN
F 3 "" H 5300 5000 50  0001 C CNN
	1    5300 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR050
U 1 1 5B10ED69
P 5750 3600
F 0 "#PWR050" H 5750 3350 50  0001 C CNN
F 1 "GND" H 5750 3450 50  0000 C CNN
F 2 "" H 5750 3600 50  0001 C CNN
F 3 "" H 5750 3600 50  0001 C CNN
	1    5750 3600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5300 4150 5300 4500
Wire Wire Line
	5300 4800 5300 5000
Wire Wire Line
	5750 3300 5750 3600
Wire Wire Line
	5150 3300 5150 3750
Wire Wire Line
	4850 3650 5300 3650
Connection ~ 5150 3650
Text GLabel 2050 3550 0    60   Input ~ 0
3.3V
$Comp
L MPU-9250 U5
U 1 1 5B1183A0
P 4050 4350
F 0 "U5" H 3450 5200 50  0000 L BNN
F 1 "MPU-9250" H 4300 5200 50  0000 L BNN
F 2 "Housings_DFN_QFN:QFN-24_3x3mm_Pitch0.4mm_NoMask" H 4050 4350 50  0001 L BNN
F 3 "SMD Gyroscope/Accelerometer/Magnetometer Sensor; 9-AXIS" H 4050 4350 50  0001 L BNN
F 4 "1428-1019-1-ND" H 4050 4350 50  0001 L BNN "Campo4"
F 5 "QFN-24 InvenSense" H 4050 4350 50  0001 L BNN "Campo5"
F 6 "TDK-InvenSense" H 4050 4350 50  0001 L BNN "Campo6"
F 7 "https://www.digikey.com/product-detail/en/tdk-invensense/MPU-9250/1428-1019-1-ND/4626450?utm_source=snapeda&utm_medium=aggregator&utm_campaign=symbol" H 4050 4350 50  0001 L BNN "Campo7"
F 8 "MPU-9250" H 4050 4350 50  0001 L BNN "Campo8"
	1    4050 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 3300 5250 3300
Wire Wire Line
	5550 3300 5750 3300
Wire Wire Line
	5150 3750 4850 3750
Text GLabel 5300 3950 2    60   Output ~ 0
IMU_INT1
Wire Wire Line
	5300 3950 4850 3950
NoConn ~ 4850 4450
NoConn ~ 4850 4950
$Comp
L GND #PWR051
U 1 1 5B1191E7
P 5100 5000
F 0 "#PWR051" H 5100 4750 50  0001 C CNN
F 1 "GND" H 5100 4850 50  0000 C CNN
F 2 "" H 5100 5000 50  0001 C CNN
F 3 "" H 5100 5000 50  0001 C CNN
	1    5100 5000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 4550 5100 5000
Wire Wire Line
	5100 4850 4850 4850
Wire Wire Line
	5300 4150 4850 4150
Text GLabel 3050 3950 0    60   Output ~ 0
FSYNC
Wire Wire Line
	3250 3950 3050 3950
Text GLabel 2000 4150 0    60   BiDi ~ 0
SENSORES_CS
Wire Wire Line
	2200 3950 2200 4150
Wire Wire Line
	2000 4150 3250 4150
Wire Wire Line
	2000 4350 3250 4350
Connection ~ 2200 4150
$Comp
L GND #PWR052
U 1 1 5B11A3A6
P 2800 5050
F 0 "#PWR052" H 2800 4800 50  0001 C CNN
F 1 "GND" H 2800 4900 50  0000 C CNN
F 2 "" H 2800 5050 50  0001 C CNN
F 3 "" H 2800 5050 50  0001 C CNN
	1    2800 5050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3250 4450 2800 4450
Wire Wire Line
	2800 4450 2800 5050
NoConn ~ 3250 4550
NoConn ~ 3250 4650
Text Notes 1150 5400 0    60   ~ 0
NOTA: AD0 a masa setea adress 0x68. AD0 a VDD setea adress a 0x69
Text Notes 3450 5150 0    60   ~ 0
Address: 0x68\n
Wire Notes Line
	600  5600 6950 5600
Text HLabel 3600 2150 0    60   BiDi ~ 0
SENSORES_CS
Text GLabel 3800 2150 2    60   BiDi ~ 0
SENSORES_CS
Wire Wire Line
	3800 2150 3600 2150
Text GLabel 3750 1600 2    60   Input ~ 0
FSYNC
Text HLabel 3550 1600 0    60   Output ~ 0
FSYNC
Wire Wire Line
	3550 1600 3750 1600
$Comp
L R R19
U 1 1 5B11709C
P 2200 3800
F 0 "R19" V 2280 3800 50  0000 C CNN
F 1 "10K" V 2200 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2130 3800 50  0001 C CNN
F 3 "" H 2200 3800 50  0001 C CNN
	1    2200 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4250 3250 4250
Wire Wire Line
	2050 3550 2200 3550
Wire Wire Line
	2200 3550 2200 3650
Wire Notes Line
	6950 2800 600  2800
Wire Notes Line
	6950 600  6950 7650
Text GLabel 5000 4350 2    60   Input ~ 0
3.3V
Wire Wire Line
	5000 4350 4850 4350
Wire Wire Line
	4850 4550 5100 4550
Connection ~ 5100 4850
$EndSCHEMATC
